import Vue from 'vue'
import App from "./layout/App"
import router from './routes'
import store from './store'
import { BootstrapVue, BootstrapVueIcons } from 'bootstrap-vue'
import axios from 'axios'
import VueAxios from 'vue-axios'
import {registeredFilters} from './filters'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import VueAuth from '@websanova/vue-auth'
import auth from './auth'
import Toasted from 'vue-toasted';
import VueConfirmDialog from 'vue-confirm-dialog'

// add custom filters
registeredFilters()

Vue.router = router

Vue.use(VueAxios, axios)
const port = window.location.port === '' ? '' : ':' + window.location.port
axios.defaults.baseURL = window.location.protocol + '//' + window.location.hostname + port + '/api'

Vue.use(VueAuth, auth)
Vue.use(BootstrapVue)
Vue.use(BootstrapVueIcons)
Vue.use(VueConfirmDialog)
Vue.use(Toasted, {duration: 10000});

Vue.config.productionTip = false;

// Response
let toast = Vue.toasted
toast.options.position = 'top-center'
toast.options.duration = 1500
const currentPage = router.currentRoute.path !== '/app/login'

// interceptors for show toast notification based on response
axios.interceptors.response.use((response) => {
    const methods = ['put', 'post', 'delete']
    const method = response.config.method.toLowerCase()
    if (methods.includes(method)) {
        toast.success('Başarılı')
    }
    return response;
}, (error) => {
    if (error.response.status === 401) {
        if (currentPage) {
            Vue.auth.logout({
                redirect: {
                    name: 'Login'
                }
            })
        }
        toast.error('Kullanıcı Doğrulanamadı')
    } else if (error.response.status === 422) {
        toast.error('Verilen veriler doğru değil!')
    }
});

new Vue({
    el: '#app',
    store,
    router,
    render: h => h(App),
});
