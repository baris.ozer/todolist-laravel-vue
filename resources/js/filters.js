import Vue from 'vue';
import moment from 'moment';

export const FILTERS = {
    // a pipe for the date format
    date: (value) => {
        const rawDate = moment(value).lang('tr')
        return rawDate.fromNow()
    }
}

export const registeredFilters = function () {
    Object.keys(FILTERS).forEach(key => {
        Vue.filter(key, FILTERS[key]);
    })
}
