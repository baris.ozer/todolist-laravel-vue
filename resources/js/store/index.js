import Vue from 'vue';
import Vuex from 'vuex';

// todolist state
import todo from "./todo";

Vue.use(Vuex);
// State is provided through a single js file. The goal is to avoid code complexity
export default new Vuex.Store({
    modules: {
        todo
    },
});
