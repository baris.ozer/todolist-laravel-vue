import axios from 'axios'

const resource = '/auth/todo'

export default {
    namespaced: true,

    state: {
        todos: {},
    },

    getters: {
        todos: state => state.todos,
    },

    mutations: {
        setTodos(state, todos) {
            state.todos = todos
        }
    },

    actions: {
        async getTodos({commit}, {query = ''}) {
            const q = query.length > 0 ? `?${query}` : ''
            const {data: todos} = await axios.get(`${resource}${q}`)

            commit('setTodos', todos);
        },
        async createTodo({commit}, payload) {
            return axios.post(`${resource}`, payload)
        },
        async deleteTodo({commit}, id) {
            return axios.delete(`${resource}/${id}`)
        },
        async updateTodo({commit}, {id, payload}) {
            return axios.put(`${resource}/${id}`, payload)
        },
    },
};
