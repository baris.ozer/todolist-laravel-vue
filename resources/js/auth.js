import bearer from '@websanova/vue-auth/drivers/auth/bearer'
import axios from '@websanova/vue-auth/drivers/http/axios.1.x'
import router from '@websanova/vue-auth/drivers/router/vue-router.2.x'

const config = {
    auth: bearer,
    http: axios,
    router: router,
    tokenDefaultName: 'auth',
    tokenStore: ['localStorage', 'cookie'],
    authRedirect: {
        name: 'Login'
    },
    notFoundRedirect: {
        name: 'notFound'
    },
    registerData: {
        url: 'auth/register',
        method: 'POST',
        redirect: '/login'
    },
    loginData: {
        url: 'auth/login',
        method: 'POST',
        redirect: {
            name: 'Todos'
        },
        fetchUser: true
    },
    logoutData: {
        url: 'auth/logout',
        method: 'POST',
        redirect: {
            name: 'Login'
        },
        makeRequest: false
    },
    fetchData: {
        url: 'auth/me',
        method: 'GET',
        enabled: true
    },
    refreshData: {
        url: 'auth/refresh',
        method: 'GET',
        enabled: true,
        interval: 5
    }
}

export default config
