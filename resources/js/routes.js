import Vue from 'vue'
import VueRouter from 'vue-router'

import App from "./layout/App";
import Login from "./pages/Login/Login";
import Todo from "./pages/Todo/Index";
import NotFound from "./pages/NotFound/Index";

Vue.use(VueRouter)

const router = new VueRouter({
    mode: 'history',
    routes: [
        {
            path: '/app',
            component: App,
            children: [
                {
                    path: 'login',
                    name: 'Login',
                    meta: {
                        auth: false
                    },
                    component: Login,
                },
                {
                    path: 'todos',
                    name: 'Todos',
                    component: Todo,
                    meta: {
                        auth: true
                    }
                },
                 { path: '*', component: NotFound, name: 'notFound' }
            ]
        },
    ],
});

export default router
