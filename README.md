# Todolist Laravel Vue

## Kurulum 

### Gereksinimler

* [Docker]
* [Docker-compose]
* [PHP 7.x^]
* [Postgresql]

```sh 
git clone https://gitlab.com/baris.ozer/todolist-laravel-vue.git
```

##### Adımlar
```sh
cd todolist-laravel-vue
cp .env.example .env
composer install
npm install
docker-compose up -d => "/etc/hosts dosyası içersinde 127.0.0.1 postgres tanımlaması yapılmalı"
hosts dosyası için => "echo '127.0.0.1 postgres' | sudo tee -a /etc/hosts"
php artisan migrate:fresh --seed 
npm run watch
php artisan serve
```

