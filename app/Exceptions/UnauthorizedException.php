<?php

namespace App\Exceptions;

use Exception;

class UnauthorizedException extends Exception
{
    protected $message = 'Kullanıcı Doğrulanamadı';
    protected $code = 401;

    /**
     * @param $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function render($request)
    {
        return response()->json(['error' => ['message' => $this->message]], $this->code);
    }
}
