<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;

class FrontEndController extends Controller
{
    /**
     * start view for spa structure
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index()
    {
        return view('app');
    }
}
