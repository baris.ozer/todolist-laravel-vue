<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Todo\StoreRequest;
use App\Http\Requests\Todo\UpdateRequest;
use App\Http\Resources\TodoResource;
use App\Repositories\TodoRepositoryEloquent;
use Carbon\Carbon;
use Illuminate\Http\Request;

class TodoController extends Controller
{
    protected $todoRepository;

    /**
     * TodoController constructor.
     * Url queries are parsed with repositories
     * @param TodoRepositoryEloquent $todoRepositoryEloquent
     */
    public function __construct(TodoRepositoryEloquent $todoRepositoryEloquent)
    {
        $this->todoRepository = $todoRepositoryEloquent;
    }

    /**
     * List all todos
     * @param Request $request
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator|\Illuminate\Support\Collection|mixed
     */
    public function index(Request $request)
    {
        $todos = $this->todoRepository->paginate($request->get('paginate'));

        return TodoResource::collection($todos);
    }

    /**
     * @param StoreRequest $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function store(StoreRequest $request)
    {
        $attributes = $request->only('content', 'priority');
        $attributes['user_id'] = auth()->user()->id;

        $this->todoRepository->create($attributes);

        return response()->json([
            'status' => 'success'
        ]);
    }

    /**
     * @param UpdateRequest $request
     * @param string $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(UpdateRequest $request, string $id)
    {
        $todo = $this->todoRepository
            ->findWhere([
                'uuid' => $id,
                'user_id' => auth()->user()->id
            ])->first();

        // new values will be added to this array
        $fields = [];

        if ($request->filled('content')) {
            $fields['content'] = $request->get('content');
        }

        if ($request->filled('is_finish')) {
            $fields['is_finished'] = $request->get('is_finish') ? Carbon::now()->toDate() : null;
        }

        if ($request->filled('priority')) {
            $fields['priority'] = $request->get('priority');
        }

        $todo->update($fields);

        return response()->json([
            'status' => 'success'
        ]);
    }

    /**
     * delete todoItem with uuid
     * @param string $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(string $id)
    {
        $this->todoRepository->deleteWhere([
            'uuid' => $id,
            'user_id' => auth()->user()->id
        ]);

        return response()->json([
            'status' => 'success',
        ]);
    }
}
