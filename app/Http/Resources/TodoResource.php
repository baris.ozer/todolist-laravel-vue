<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class TodoResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => $this->uuid,
            "content" => $this->content,
            "is_finish" => $this->is_finished,
            "priority" => $this->priority,
            "created_at" => $this->created_at
        ];
    }
}
