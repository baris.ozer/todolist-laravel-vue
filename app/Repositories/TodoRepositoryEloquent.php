<?php

namespace App\Repositories;

use App\Models\Todo;
use Prettus\Repository\Criteria\RequestCriteria;
use Prettus\Repository\Eloquent\BaseRepository;

/**
 * Class TodoRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class TodoRepositoryEloquent extends BaseRepository implements TodoRepository
{
    /**
     * @var string[]
     */
    protected $fieldSearchable = [
        'content' => 'ilike',
    ];

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Todo::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

}
